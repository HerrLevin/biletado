<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @method static create(mixed $values)
 * @method static findOrFail($value)
 * @method static find($storey_id)
 * @method static where(string $string, mixed $building_id)
 */
class Storey extends Model
{
    protected $table = 'storeys';
    public $timestamps = false;
    protected $fillable = ['name', 'building_id'];
    protected $primaryKey = "id";
    protected $keyType = 'string';

    public function building(): HasOne
    {
        return $this->hasOne(related: Building::class, foreignKey: 'id', localKey: 'building_id');
    }

    public function rooms(): HasMany
    {
        return $this->hasMany(related: Room::class, foreignKey: 'storey_id', localKey: 'id');
    }
}
