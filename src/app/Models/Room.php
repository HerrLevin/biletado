<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Log;
use JsonException;

/**
 * @method static create(mixed $values)
 * @method static findOrFail($value)
 * @method static where(string $string, mixed $storey_id)
 * @property mixed $id
 */
class Room extends Model
{
    protected $table = 'rooms';
    public $timestamps = false;
    protected $fillable = ['name', 'storey_id'];
    protected $primaryKey = "id";
    protected $keyType = 'string';

    public function storey(): HasOne
    {
        return $this->hasOne(related: Storey::class, foreignKey: 'id', localKey: 'storey_id');
    }

    /**
     * Returns the reservations count for the current room
     * @throws GuzzleException
     */
    public function reservationsCount(): int
    {
        try {
            $client = new Client([
                //'base_uri' => 'http://'.env(key: 'RESERVATIONS_HOST', default: 'traefik').'/api/',
                'base_uri' => 'http://traefik/api/',
                'headers' => [
                    env(key: 'JAEGER_TRACECONTEXTHEADERNAME', default: 'uber-trace-id') => session(key: 'jaeger-token')
                ]
            ]);
            $uri = new Uri(env(key: 'RESERVATIONS_PATH', default: 'reservations/'));


            $params = ['room_id' => $this->id];

            $response = $client->request(
                method: 'GET',
                uri: $uri->withQuery(query: http_build_query(data: $params)),
                options: [
                    'verify'  => false,
                ]
            );
        } catch (ClientException $exception) {
            Log::error(message: "GuzzleHttp ClientException: " . $exception);
            die();
        }

        try {
            $responseBody = json_decode($response->getBody(), associative: true, depth: 512, flags: JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            Log::error(message: "Returned Json is malformed. Exception: " . $exception);
            return 0;
        }

        /**
         * This is a hotfix due to wrong API implementations
         * @todo replace this as soon as the API is fixed
         */
        if ($response->getStatusCode() === 404 || is_null(value: $responseBody)) {
            return 0;
        }

        $cnt = 0;
        foreach ($responseBody as $reservation) {
            if ($reservation['room_id'] === $this->id) {
                $cnt++;
            }
        }

        return $cnt;
    }
}
