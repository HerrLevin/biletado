<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static create(mixed $values)
 * @method static findOrFail($value)
 * @method static find($building_id)
 */
class Building extends Model
{
    protected $table = 'buildings';
    public $timestamps = false;
    protected $fillable = ['name', 'address'];
    protected $primaryKey = "id";
    protected $keyType = 'string';

    public function storeys(): HasMany
    {
        return $this->hasMany(related: Storey::class, foreignKey: 'building_id', localKey: 'id');
    }
}
