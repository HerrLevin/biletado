<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;

class JaegerGetTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse|JsonResponse
    {
        $jaegerTraceContextHeaderName = env(key: 'JAEGER_TRACECONTEXTHEADERNAME', default: 'uber-trace-id');

        session(['jaeger-token'=> '']);
        if ($request->hasHeader(key: $jaegerTraceContextHeaderName)) {
            session(['jaeger-token' => $request->header(key: $jaegerTraceContextHeaderName)]);
        }
        Log::debug(message: 'Jaeger-Token: ' . $request->header(key: $jaegerTraceContextHeaderName));
        return $next($request);
    }
}
