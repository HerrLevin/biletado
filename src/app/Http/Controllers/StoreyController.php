<?php

namespace App\Http\Controllers;

use App\Http\Resources\StoreyResource;
use App\Models\Building;
use App\Models\Storey;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StoreyController extends Controller
{
    /**
     * Validation rules for POST/PUT requests
     * @var array|string[]
     */
    private array $validationRules = [
        'id' => 'uuid|nullable',
        'name' => 'required',
        'building_id' => 'uuid|required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (isset($request['building_id'])) {
            return $this->returnStoreys(Storey::where('building_id', $request['building_id'])->get());
        }

        return $this->returnStoreys(Storey::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $request->validate($this->validationRules);
        $status = 201;
        // Abort if supplied building is non-existent
        if (!Building::find($validated['building_id'])) {
            abort(422);
        }

        if (isset($validated['id'])) {
            $storey = Storey::findOrFail($validated['id']);
            $storey->update($validated);
            $status = 200;
        }
        return $this->returnStorey(storey: Storey::create($validated), statusCode: $status);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        return $this->returnStorey(Storey::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $id
     * @return void
     */
    public function update(Request $request, string $id): void
    {
        $storey = Storey::findOrFail($id);
        $validated = $request->validate($this->validationRules);

        if ((isset($validated['id']) && $id !== $validated['id']) || !Building::find($validated['building_id'])) {
            abort(code: 422);
        }

        $storey->update($validated);
        abort(code: 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return void
     */
    public function destroy(string $id): void
    {
        $storey = Storey::findOrFail($id);

        if ($storey->rooms->count() > 0) {
            abort(422);
        }

        $storey->delete();
        abort(204);
    }

    /**
     * Return single StoreyResource with an HTTP response body and predefined status code
     * @param Storey $storey
     * @param int $statusCode
     * @return JsonResponse
     */
    private function returnStorey(Storey $storey, int $statusCode=200): JsonResponse
    {
        return (new StoreyResource($storey))->response()->setStatusCode($statusCode);
    }

    /**
     * Return StoreyResource collection with an HTTP response body and predefined status code
     * @param mixed $storeys
     * @param int $statusCode
     * @return JsonResponse
     */
    private function returnStoreys(mixed $storeys, int $statusCode=200): JsonResponse
    {
        return (StoreyResource::collection(resource: $storeys))->response()->setStatusCode($statusCode);
    }
}
