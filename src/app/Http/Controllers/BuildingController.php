<?php

namespace App\Http\Controllers;

use App\Http\Resources\BuildingResource;
use App\Models\Building;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BuildingController extends Controller
{
    /**
     * Validation rules for POST/PUT requests
     * @var array|string[]
     */
    private array $validationRules = [
        'id' => 'uuid|nullable',
        'name' => 'required',
        'address' => 'nullable'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->returnBuildings(Building::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $request->validate($this->validationRules);
        $status = 201;

        if (isset($validated['id'])) {
            $building = Building::findOrFail($validated['id']);
            $building->update($validated);
            $status = 200;
        }
        return $this->returnBuilding(building: Building::create($validated), statusCode: $status);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        return $this->returnBuilding(Building::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $id
     * @return void
     */
    public function update(Request $request, string $id): void
    {
        $building = Building::findOrFail($id);
        $validated = $request->validate($this->validationRules);

        if (isset($validated['id']) && $id !== $validated['id']) {
            abort(code: 422);
        }

        $building->update($validated);
        abort(code: 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return void
     */
    public function destroy(string $id): void
    {
        $building = Building::findOrFail($id);

        if ($building->storeys->count() > 0) {
            abort(422);
        }

        $building->delete();
        abort(204);
    }

    /**
     * Return single BuildingResource with an HTTP response body and predefined status code
     * @param Building $building
     * @param int $statusCode
     * @return JsonResponse
     */
    private function returnBuilding(Building $building, int $statusCode=200): JsonResponse
    {
        return (new BuildingResource($building))->response()->setStatusCode($statusCode);
    }

    /**
     * Return BuildingResource collection with an HTTP response body and predefined status code
     * @param mixed $buildings
     * @param int $statusCode
     * @return JsonResponse
     */
    private function returnBuildings(mixed $buildings, int $statusCode=200): JsonResponse
    {
        return (BuildingResource::collection(resource: $buildings))->response()->setStatusCode($statusCode);
    }
}
