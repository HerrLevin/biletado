<?php

namespace App\Exceptions;

use App\Http\Resources\JsonErrorResource;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use \Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Psy\Util\Json;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


    public function render($request, Exception|Throwable $exception)
    {
        if ($exception instanceof HttpException) {
            return $this->returnError(response_code: $exception->getStatusCode());
        }

        if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
            return $this->returnError();
        }

        if ($exception instanceof ValidationException) {
            return $this->returnError(response_code: 400);
        }

        return parent::render($request, $exception);
    }

    private function returnError($response_code = 404, $resource = []): JsonResponse
    {
        return (new JsonErrorResource(resource: $resource))->response()->setStatusCode(code: $response_code);
    }
}
