<?php

use App\Http\Controllers\BuildingController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\StoreyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'assets'], function() {
    Route::group(['prefix' => 'storeys'], function() {
        Route::get(uri: '/', action: [StoreyController::class, 'index']);
        Route::get(uri: '/{id}', action: [StoreyController::class, 'show']);
        Route::group(['middleware' => 'jwt'], function() {
            Route::post(uri: '/', action: [StoreyController::class, 'store']);
            Route::put(uri: '/{id}', action: [StoreyController::class, 'update']);
            Route::delete(uri: '/{id}', action: [StoreyController::class, 'destroy']);
        });
    });

    Route::group(['prefix' => 'rooms'], function() {
        Route::get(uri: '/', action: [RoomController::class, 'index']);
        Route::get(uri: '/{id}', action: [RoomController::class, 'show']);
        Route::group(['middleware' => 'jwt'], function() {
            Route::post(uri: '/', action: [RoomController::class, 'store']);
            Route::put(uri: '/{id}', action: [RoomController::class, 'update']);
            Route::delete(uri: '/{id}', action: [RoomController::class, 'destroy']);
        });
    });

    Route::group(['prefix' => 'buildings'], function() {
        Route::get(uri: '/', action: [BuildingController::class, 'index']);
        Route::get(uri: '/{id}', action: [BuildingController::class, 'show']);
        Route::group(['middleware' => 'jwt'], function() {
            Route::post(uri: '/', action: [BuildingController::class, 'store']);
            Route::put(uri: '/{id}', action: [BuildingController::class, 'update']);
            Route::delete(uri: '/{id}', action: [BuildingController::class, 'destroy']);
        });
    });
});
