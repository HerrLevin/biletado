#!/bin/sh

cd /var/www

if ! [[test -f .env]]; then
  cp .env.example .env
  php artisan key:generate
fi

# php artisan migrate:fresh --seed
#php artisan cache:clear
#php artisan route:cache

/usr/bin/supervisord -c /etc/supervisord.conf